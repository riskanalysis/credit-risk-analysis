package com.creditrisk.analysis.VO;

import com.creditrisk.analysis.model.CreditRiskRating;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseTemplateVO {
	private CreditProposal proposal;
	private CreditRiskRating riskRating;
}
