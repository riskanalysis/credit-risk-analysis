package com.creditrisk.analysis.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.creditrisk.analysis.model.CreditRiskRating;

public interface CreditRiskAnalysisRepository extends JpaRepository<CreditRiskRating, Long>{

	CreditRiskRating findByProposalId(Long proposalId);

}
