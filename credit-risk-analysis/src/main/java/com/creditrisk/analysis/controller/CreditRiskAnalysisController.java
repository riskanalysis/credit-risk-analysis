package com.creditrisk.analysis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.creditrisk.analysis.model.CreditRiskRating;
import com.creditrisk.analysis.service.CreditRiskAnalysisService;

@RestController
@RequestMapping(path = "/api/v1/creditriskanalysis", produces = "application/json")
public class CreditRiskAnalysisController {
	@Autowired
	CreditRiskAnalysisService service;

	@GetMapping("/generateRiskRating/{proposalId}")
	public ResponseEntity<CreditRiskRating> generateRiskAnalysisRatingForProposal(@PathVariable Long proposalId) {
		CreditRiskRating creditRiskRating = this.service.generateRiskAnalysisRatingForProposal(proposalId);
		return ResponseEntity.ok(creditRiskRating);
	}

	@PostMapping("/saveRiskRating")
	public ResponseEntity<CreditRiskRating> saveRiskRating(@RequestBody CreditRiskRating riskRating) {
		return ResponseEntity.ok(this.service.saveRiskRating(riskRating));
	}
}
