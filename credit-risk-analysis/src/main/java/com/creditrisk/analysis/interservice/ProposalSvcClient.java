package com.creditrisk.analysis.interservice;

import org.springframework.web.bind.annotation.PathVariable;

import com.creditrisk.analysis.VO.CreditProposal;

public interface ProposalSvcClient {
	public CreditProposal getProposalById(@PathVariable Long id);
}
