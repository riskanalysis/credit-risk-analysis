package com.creditrisk.analysis.interservice.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.creditrisk.analysis.VO.CreditProposal;
import com.creditrisk.analysis.interservice.ProposalSvcClient;

@FeignClient(name = "proposalSvcClient", url = "${interservice.proposalsvc.url}")
public interface ProposalSvcFeignClient extends ProposalSvcClient {

	@GetMapping(value = "/getproposal/{id}")
	public CreditProposal getProposalById(@PathVariable("id") Long id);

}
