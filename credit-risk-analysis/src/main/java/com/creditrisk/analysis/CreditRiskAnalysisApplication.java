package com.creditrisk.analysis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = { "com.creditrisk.analysis.repository" })
@EntityScan(basePackages = { "com.creditrisk.analysis.model" })
@EnableFeignClients
public class CreditRiskAnalysisApplication {
	public static void main(String[] args) {
		SpringApplication.run(CreditRiskAnalysisApplication.class, args);
	}
	
}
