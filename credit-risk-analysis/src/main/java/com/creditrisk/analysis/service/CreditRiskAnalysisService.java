package com.creditrisk.analysis.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.creditrisk.analysis.VO.CreditProposal;
import com.creditrisk.analysis.interservice.ProposalSvcClient;
import com.creditrisk.analysis.model.CreditRiskRating;
import com.creditrisk.analysis.repository.CreditRiskAnalysisRepository;

@Service
public class CreditRiskAnalysisService {
	@Autowired
	CreditRiskAnalysisRepository repository;
	
	@Autowired
	ProposalSvcClient proposalSvcClient   ;

	public CreditRiskRating saveRiskRating(CreditRiskRating riskRating) {
		return repository.save(riskRating);
	}

	public CreditRiskRating generateRiskAnalysisRatingForProposal(Long proposalId) {
		CreditProposal creditProposal= this.proposalSvcClient.getProposalById(proposalId);
		return this.calcualteRiskRating(creditProposal);
	}

	public CreditRiskRating calcualteRiskRating(CreditProposal proposal) {
		CreditRiskRating riskRating = new CreditRiskRating();
		riskRating.setFrTotal(proposal.getFrParam1()+proposal.getFrParam2()+proposal.getFrParam3());
		riskRating.setBrTotal(proposal.getBrParam1()+proposal.getBrParam2());
		riskRating.setTotalRisk(riskRating.getFrTotal()+riskRating.getBrTotal());
		riskRating.setRiskRating("SB-10");
		riskRating.setRiskCategory("HighRisk");
		return riskRating;
	}

}
