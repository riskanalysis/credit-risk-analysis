package com.creditrisk.analysis.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@AllArgsConstructor
@NoArgsConstructor
public class CreditRiskRating {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long Id;
	public Long frTotal;
	public Long brTotal;
	public Long totalRisk;
	public String riskRating;
	public String riskCategory;
	public Long proposalId;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public Long getFrTotal() {
		return frTotal;
	}

	public void setFrTotal(Long frTotal) {
		this.frTotal = frTotal;
	}

	public Long getBrTotal() {
		return brTotal;
	}

	public void setBrTotal(Long brTotal) {
		this.brTotal = brTotal;
	}

	public Long getTotalRisk() {
		return totalRisk;
	}

	public void setTotalRisk(Long totalRisk) {
		this.totalRisk = totalRisk;
	}

	public String getRiskRating() {
		return riskRating;
	}

	public void setRiskRating(String riskRating) {
		this.riskRating = riskRating;
	}

	public String getRiskCategory() {
		return riskCategory;
	}

	public void setRiskCategory(String riskCategory) {
		this.riskCategory = riskCategory;
	}

	public Long getProposalId() {
		return proposalId;
	}

	public void setProposalId(Long proposalId) {
		this.proposalId = proposalId;
	}

}
